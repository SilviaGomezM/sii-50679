//Autor:Silvia Gómez Martín
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"


	int main()
	{

	//Creacion del fichero,puntero a memoria y el puntero para la proyeccion
	int fdbot;
	DatosMemCompartida* pDatos;
	char* proyeccion;

	//Apertura del fichero

	fdbot=open("/tmp/datosBot.txt",O_RDWR);

	//Proyecto el fichero

	proyeccion=(char*)mmap(NULL,sizeof(*(pDatos)),PROT_WRITE|PROT_READ,MAP_SHARED,fdbot,0);


	//Cierre del fichero

	close(fdbot);


	//Apunto el puntero de Datos a la proyeccion del fichero en memoria

	pDatos=(DatosMemCompartida*)proyeccion;



	//Acciones de control de la raqueta

	while(1)
	{

		usleep(25000);  
		//movimiento raqueta 1
		if(((pDatos->raqueta1.y1+pDatos->raqueta1.y2)/2)<pDatos->esfera.centro.y) pDatos->accion1=1;
		else if(((pDatos->raqueta1.y1+pDatos->raqueta1.y2)/2)>pDatos->esfera.centro.y) pDatos->accion1=-1;
		else pDatos->accion1=0;
		//movimiento raqueta 2
		if(((pDatos->raqueta2.y1+pDatos->raqueta2.y2)/2)<pDatos->esfera.centro.y) pDatos->accion2=1;
		else if(((pDatos->raqueta2.y1+pDatos->raqueta2.y2)/2)>pDatos->esfera.centro.y) pDatos->accion2=-1;
		else pDatos->accion2=0;

	}

	//Se desmonta la proyeccion de memoria

	munmap(proyeccion,sizeof(*(pDatos)));

	}
