//Autor:Silvia Gómez Martín
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
//Lee del fichero cadena de texto
int main(int argc, char* argv[]) {
	
	int salir=0;
	int rd;

	int mk=mkfifo("/tmp/loggerfifo",0777);
	if(mk==-1){perror("Error: Creacion FIFO");}

	int fdlog=open("/tmp/loggerfifo", O_RDONLY);
	if(fdlog==-1){perror("Error: Apertura FIFO");}
	
	char buff[200];
	while(read(fdlog,buff,sizeof(buff)))
	{
		printf("%s\n", buff);

		if((buff[0]=='E')||(rd==-1)) //Chequeas si se produce error también
		{

			printf("----Tenis cerrado. Cerrando logger...--- \n");
			break;
		}
	}
	int clog=close(fdlog);
	if(clog==-1){perror("Error: Cerrar el FIFO");}

	unlink("/tmp/loggerfifo");
	return 0;
}

