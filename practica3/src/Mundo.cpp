// Mundo.cpp: implementation of the CMundo class.
//Autor:Silvia Gómez Martín
//////////////////////////////////////////////////////////////////////

#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
char* proyeccion;
static int contador=0;
static int estadocontrol=0;

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//Cierre del fifo
	int clog=close(fdlog);
	if(clog==-1){perror("Error: Cerrar tuberia logger en Mundo");}

 	//Cierre de la proyección de memoria
	munmap(proyeccion,sizeof(Datos));
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	//incremento contador espera de la raqueta 2
	contador++;

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		//mensaje tuberia FIFO
		char cad[200];
		sprintf(cad,"Jugador 2 marca 1 punto, lleva un total de %d puntos", puntos2);
		int wlog=write(fdlog,cad,strlen(cad)+1);
		if(wlog==-1){perror("Error: Escritura en logger");}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		//mensaje tuberia FIFO
		char cad[200];
		sprintf(cad,"Jugador 1 marca 1 punto, lleva un total de %d puntos", puntos1);
		int wlog=write(fdlog,cad,strlen(cad)+1);
		if(wlog==-1){perror("Error: Escritura en logger");}
	}


	//actualizacion de los valores reales en nuestra proyección del fichero
	pDatos->esfera=esfera;
	pDatos->raqueta1=jugador1;
	pDatos->raqueta2=jugador2;

	//Implementación sobre el bot

	switch(pDatos->accion1)
	{
		case 1: {OnKeyboardDown('w',0,0); break;}
		case 0: break;
		case -1:{OnKeyboardDown('s',0,0);break;}
	}

	if(contador>=450)estadocontrol=1;
	if(estadocontrol==1)
	{
		switch(pDatos->accion2)

		{

			case 1: {OnKeyboardDown('o',0,0); break;}
			case 0: break;
			case -1:{OnKeyboardDown('l',0,0);break;}

		}	
	
	}
	
	//Ejercicio Extra ganar quien llegue a 3 puntos
	if(puntos1==3)
	{
		char cad3[200];
		sprintf(cad3,"El jugador 1 ha llegado a %d puntos, ¡ha ganado la partida!",puntos1);
		int wlog=write(fdlog,cad3,sizeof(cad3));
		if(wlog==-1){perror("Error: Escritura en logger");}
	}
	if(puntos2==3)
	{
		char cad4[200];
		sprintf(cad4,"El jugador 2 ha llegado a %d puntos, ¡ha ganado la partida!",puntos2);
		int wlog=write(fdlog,cad4,sizeof(cad4));
		if(wlog==-1){perror("Error: Escritura en logger");}
	}
		
}
void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	//case 'a':jugador1.velocidad.x=-1;break;
	//case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':{jugador2.velocidad.y=-4;contador=0;break;}
	case 'o':{jugador2.velocidad.y=4;contador=0;break;}
	}
	
}


void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;



//Informacion sobre el Logger
	fdlog=open("/tmp/loggerfifo",O_WRONLY);
	if(fdlog==-1){perror("Error: Apertura logger en mundo");}

//Para el bot
//Fichero proyectado en memoria

	fdbot=open("/tmp/datosBot.txt",O_RDWR|O_CREAT|O_TRUNC, 0777); //Creacion y apertura del fichero
	if(fdbot==-1){perror("Error: Apertura del fichero bot");}

	int wbot=write(fdbot,&Datos,sizeof(Datos)); 
	if(wbot==-1){perror("Error: Escritura en el fichero bot");}

	proyeccion=(char*)mmap(NULL,sizeof(Datos),PROT_WRITE|PROT_READ,MAP_SHARED,fdbot,0); //Asignamos al puntero de proyeccion el lugar donde esta proyectado el fichero
	int cbot=close(fdbot); 
	if(cbot==-1){perror("Error: Cerrar el fichero bot");}

	pDatos=(DatosMemCompartida*)proyeccion; //Asignamos el valor del puntero de proyeccion al puntero de Datos

	pDatos->accion1=0;
	pDatos->accion2=0;
}
